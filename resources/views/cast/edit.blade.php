@extends('templates.master')
@section('judul')
Edit Pemeran {{$caster->nama}}
@endsection

@section('content')

    <form action="/cast/{{$caster->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" name="nama" value= "{{$caster->nama}}" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
        <label>Usia</label>
        <input name="umur" class="form-control" value="{{$caster->umur}}">
        </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
        <label>Biografi Cast</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$caster->bio}}</textarea>
        </div>
        @error('biografi')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection