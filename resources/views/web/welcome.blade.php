@extends('templates.master')
@section('judul')
Halaman Form
@endsection

@section('content')
    <head>
        <title>Welcome</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    </head>

    <div>
        <h1>SELAMAT DATANG! {{$name1 . ' ' . $name2}}</h1>
        <h3>Terima kasih telah bergabung di Website kami. Media belajar kita bersama!</h3>
    </div>
@endsection