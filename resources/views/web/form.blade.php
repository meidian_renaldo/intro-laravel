@extends('templates.master')
@section('judul')
Halaman Form
@endsection

@section('content')
    <body>
        <div>
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>

            <form action="/welcome" method="POST">
                @csrf
                <label for="nama_depan">First Name:</label><br><br>
                <input type="text" name="nama_depan" id="nama_depan"> <br>

                <br>
                <label for="nama_belakang">Last Name:</label><br><br>
                <input type="text" name="nama_belakang" id="nama_belakang"> <br>

                <br>
                <label>Gender:</label><br><br>
                <input type="radio" name="Gender" value="G1"> Male <br>
                <input type="radio" name="Gender" value="G2"> Female <br>
                <input type="radio" name="Gender" value="G3"> Other <br>

                <br>
                <label>Nationality:</label><br><br>
                <select>
                    <option value="N1">Indonesia</option>
                    <option value="N2"> Malaysia</option>
                    <option value="N3">Brunei Darussalam</option>
                    <option value="N4">Singapura</option>
                    <option value="N5">Thailand</option>
                    <option value="N6">Laos</option>
                    <option value="N7">Myanmar</option>
                </select> <br>

                <br>
                <label>Language Spoken:</label><br><br>
                <input type="checkbox" name="Language_Spoken" value="l1"> Bahasa Indonesia <br>
                <input type="checkbox" name="Language_Spoken" value="l2"> English <br>
                <input type="checkbox" name="Language_Spoken" value="l3"> Other <br>

                <br>
                <label>Bio:</label><br><br>
                <textarea cols="30" rows="7"></textarea>

                <br> <br>
                <input type="submit" value="Sign Up">
            </form>
        </div>
    </body>
@endsection