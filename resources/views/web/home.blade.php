@extends('templates.master')
@section('judul')
Media Online
@endsection

@section('content')
    <body>
        <div>
            <h3>Sosial Media Developer</h3>
            <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
        </div>

        <div>
            <h3>Benefit Join di Media Sosial</h3>
            <ul>
                <li>Mendapat motivasi dari sesama para Developer</li>
                <li>Sharing knowledge</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
        </div>

        <div>
            <h3>Cara Bergabung ke Media Online</h3>
            <ol>
                <li>Mengunjungi website ini</li>
                <li>Mendaftar di <a href="/form" target="__blank">Form Sign Up</a></li>
                <li>Selesai</li>
            </ol>
        </div>
    </body>
@endsection