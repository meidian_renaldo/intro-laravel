<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class formController extends Controller
{
    public function form()
    {
        return view('web.form');
    }

    public function submit(Request $request)
    {
        $name1 = $request['nama_depan'];
        $name2 = $request['nama_belakang'];
        return view('web.welcome', compact('name1', 'name2'));
    }
}
