<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class castController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        $caster = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('caster'));
    }

    public function edit($id)
    {
        $caster = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('caster'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
